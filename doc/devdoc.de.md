Um Erweiterungen für das MasterPortal zu schreiben und beizutragen finden sie hier weitere Informationen.

* **[lokale Entwicklungsumgebung einrichten](setupDev.de.md)**

Es sind folgende Code-Konventionen einzuhalten:

* **[Code-Konventionen](codingConventions.de.md)**

Um Code dem Projekt beizutragen ist der Workflow und die "Definition of Done" einzuhalten:

* **[Git-Workflow](gitWorkflow.de.md)**

Zum Schreiben von Unittests:

* **[Test-Dokumentation](testing.de.md)**

Um Code zu schreiben der nicht im Masterportal-Repository landen soll, sondern als Addon eingebunden wird:

* **[Addons](addonsVue.de.md)**

Weitere Infos:

* **[Tutorial: Ein neues Modul erstellen (Scale Switcher)](vueTutorial.de.md)**
* **[Community Board (Entwickler- und Anwenderforum)](https://trello.com/c/qajdXkMa/110-willkommen)**
* **[Issue-Tracker](https://bitbucket.org/geowerkstatt-hamburg/masterportal/issues?status=new&status=open)**
